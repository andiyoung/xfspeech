#import <Flutter/Flutter.h>
#import "iflyMSC/IFlyMSC.h"

@interface XfspeechPlugin : NSObject<FlutterPlugin,IFlySpeechRecognizerDelegate,IFlyRecognizerViewDelegate,IFlyPcmRecorderDelegate,IFlyVoiceWakeuperDelegate>
@property (nonatomic,strong)  IFlyVoiceWakeuper    * iflyVoiceWakeuper;
@property (nonatomic, strong) IFlySpeechRecognizer *iFlySpeechRecognizer;
@property (nonatomic, strong) NSString *resultString;
@property (nonatomic,strong) IFlyPcmRecorder *pcmRecorder;//PCM Recorder to be used to
@end
