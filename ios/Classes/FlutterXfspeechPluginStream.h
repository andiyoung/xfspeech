//
//  FlutterXfspeechPluginStream.h
//  xfspeech
//
//  Created by 创屹研发 on 2021/12/8.
//

#import <Foundation/Foundation.h>
#import <Flutter/Flutter.h>

NS_ASSUME_NONNULL_BEGIN
@class FlutterXfspeechPluginStreamHandler;
@interface FlutterXfspeechPluginStream : NSObject
+ (instancetype)sharedInstance ;
@property (nonatomic, strong) FlutterXfspeechPluginStreamHandler* streamHandler;

@end

@interface FlutterXfspeechPluginStreamHandler : NSObject<FlutterStreamHandler>
@property (nonatomic, strong,nullable) FlutterEventSink eventSink;

@end
NS_ASSUME_NONNULL_END
