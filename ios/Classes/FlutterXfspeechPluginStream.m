//
//  FlutterXfspeechPluginStream.m
//  xfspeech
//
//  Created by 创屹研发 on 2021/12/8.
//

#import "FlutterXfspeechPluginStream.h"

@implementation FlutterXfspeechPluginStream

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    static FlutterXfspeechPluginStream *manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [[FlutterXfspeechPluginStream alloc] init];
        FlutterXfspeechPluginStreamHandler * streamHandler = [[FlutterXfspeechPluginStreamHandler alloc] init];
        manager.streamHandler = streamHandler;
    });
    
    return manager;
}

@end

@implementation FlutterXfspeechPluginStreamHandler

- (FlutterError*)onListenWithArguments:(id)arguments eventSink:(FlutterEventSink)eventSink {
    self.eventSink = eventSink;
    return nil;
}

- (FlutterError*)onCancelWithArguments:(id)arguments {
    self.eventSink = nil;
    return nil;
}

@end
