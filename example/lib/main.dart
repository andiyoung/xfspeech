import 'dart:convert';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:xfspeech/xfspeech.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  String transResult = '...';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    // We also handle the message potentially returning null.
    try {
      platformVersion =
          await Xfspeech.platformVersion ?? 'Unknown platform version';
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });

    Xfspeech().onResult().listen((event) {
      print('收到事件');
      setState(() {
        transResult = event.transResult;
        // print(event);
        // print(event.resultString);
        // print(event.transResult);
      });
      print(json.decode(event.result));
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Column(
          children: [
            Center(
              child: Text('Running on: $_platformVersion\n'),
            ),
            ElevatedButton(
                onPressed: () async {
                  await Xfspeech.init(APP_ID: '5f9d6982');
                },
                child: Text('初始化讯飞')),
            ElevatedButton(
                onPressed: () async {
                  await Xfspeech.startIvw(APP_ID: '5f9d6982');
                },
                child: Text('开始监听唤醒词')),
            ElevatedButton(
                onPressed: () async {
                  await Xfspeech.ivwStop();
                },
                child: Text('停止监听唤醒词')),

            ElevatedButton(
                onPressed: () async {
                  await Xfspeech.startIat(APP_ID: '5f9d6982', mEngineType: 'local');
                },
                child: Text('开始语音听写')),
            ElevatedButton(
                onPressed: () async {
                  await Xfspeech.iatStop();
                },
                child: Text('停止语音听写')),
            Text('识别结果：' + transResult)
          ],
        ),
      ),
    );
  }
}
